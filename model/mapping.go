package model

type Mapping struct {
	Dynamic    bool       `json:"dynamic"`
	Properties Properties `json:"properties"`
}

type Properties map[string]Field

type Field struct {
	Properties Properties `json:"properties"`
	Type       string     `json:"type"`
	Normalizer string     `json:"normalizer"`
	Fields     Fields     `json:"fields"`
}

type Fields map[string]FieldType

type FieldType struct {
	Type      string `json:"type"`
	Analyzer  string `json:"analyzer"`
	FieldData bool   `json:"fielddata"`
}
