package parser

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/mahesh/es-mapping-parser/model"
)

// UnmarshalAlias return model of alias
func UnmarshalAlias() *model.Alias {
	data, err := os.ReadFile("./sample/mapping.json")
	if err != nil {
		fmt.Println(err)
	}
	a := model.Alias{}
	json.Unmarshal(data, &a)
	return &a
}

// GetAllAliases returns list of all aliases from alias.json
func GetAllAliases(a *model.Alias) []string {
	res := make([]string, len(a.Properties))
	i := 0
	for k := range a.Properties {
		res[i] = k
		i++
	}
	return res
}

// GetAllFieldsWithAliases returns list of all fields which have aliases from alias.json
func GetAllFieldsWithAliases(a *model.Alias) []string {
	res := make([]string, len(a.Properties))
	i := 0
	for _, v := range a.Properties {
		res[i] = v.Path
		i++
	}
	return res
}

// GetFieldsToAliasMap returns map of field to list from alias.json
func GetFieldsToAliasMap(a *model.Alias) map[string]string {
	res := make(map[string]string)
	i := 0
	for k, v := range a.Properties {
		res[v.Path] = k
		i++
	}
	return res
}
