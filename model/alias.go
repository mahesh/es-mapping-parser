package model

type Alias struct {
	Properties map[string]AliasField `json:"properties"`
}

type AliasField struct {
	Path string `json:"path"`
}
