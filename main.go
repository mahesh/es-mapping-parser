package main

import (
	"fmt"

	"gitlab.com/mahesh/es-mapping-parser/parser"
)

func main() {
	a := parser.UnmarshalMappings()
	// fmt.Println(parser.GetAllAliases())
	// fmt.Println(parser.GetAllFieldsWithAliases())
	// fmt.Println(parser.GetFieldsToAliasMap())
	fmt.Println(parser.GetFieldsWithFieldData(a))
}
