package parser

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/mahesh/es-mapping-parser/model"
)

type NilType interface{}

// UnmarshalMappings return model of mappings
func UnmarshalMappings() *model.Mapping {
	data, err := os.ReadFile("./sample/mapping.json")
	if err != nil {
		fmt.Println(err)
	}
	a := model.Mapping{}
	json.Unmarshal(data, &a)
	return &a
}

// GetAllFields returns list of fields (in case of nested fields only top field is returned)
func GetAllFields(a *model.Mapping) []string {
	res := make([]string, len(a.Properties))
	i := 0
	for k := range a.Properties {
		res[i] = k
		i++
	}
	return res
}

// GetNestedFieldsMap returns map of field to list of subfields (if any)
func GetNestedFieldsMap(a *model.Mapping) map[string][]string {
	f := GetAllFields(a)
	res := map[string][]string{}
	for _, v := range f {
		vs := []string{}
		for k := range a.Properties[v].Properties {
			vs = append(vs, fmt.Sprintf("%v", k))
		}
		res[v] = vs
	}
	return res
}

// GetAllNestedFields returns list of all field (including nested fields)
func GetAllNestedFields(a *model.Mapping) []string {
	var res []string
	for k, v := range GetNestedFieldsMap(a) {
		if len(v) == 0 {
			res = append(res, k)
		} else {
			for _, sub := range v {
				res = append(res, k+"."+sub)
			}
		}
	}
	return res
}

// GetAllNestedFieldTypes returns map of all fields to their data types
func GetAllNestedFieldTypes(a *model.Mapping) map[string][]string {
	res := map[string][]string{}
	for k, v := range GetNestedFieldsMap(a) {
		if len(v) == 0 {
			t := map[string]NilType{}
			t[a.Properties[k].Type] = nil
			for _, subtype := range a.Properties[k].Fields {
				t[subtype.Type] = nil
			}
			for subt := range t {
				res[k] = append(res[k], subt)
			}
		} else {
			for _, sub := range v {
				t := map[string]NilType{}
				t[a.Properties[k].Properties[sub].Type] = nil
				for _, subtype := range a.Properties[k].Properties[sub].Fields {
					t[subtype.Type] = nil
				}
				for subt := range t {
					res[k+"."+sub] = append(res[k+"."+sub], subt)
				}
			}
		}
	}
	return res
}

// GetFieldsWithFieldData returns list of all fields with fielddata true
func GetFieldsWithFieldData(a *model.Mapping) []string {
	res := []string{}
	for k, v := range GetNestedFieldsMap(a) {
		if len(v) == 0 {
			if a.Properties[k].Fields["sort"].FieldData {
				res = append(res, k)
			}
		} else {
			for _, sub := range v {
				if a.Properties[k].Properties[sub].Fields["sort"].FieldData {
					res = append(res, k+"."+sub)
				}
			}
		}
	}
	return res
}

func GetTypeToFieldsMap(a *model.Mapping) map[string][]string {
	res := map[string][]string{}
	// implement me
	return res
}
